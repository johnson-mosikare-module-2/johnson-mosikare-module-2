void main() {
  var fnb = new Winners();
  fnb.name = "FNB";
  fnb.year = 2012;

  var snapscan = new Winners();
  snapscan.name = "SnapScan";
  snapscan.year = 2013;

  var liveinspect = new Winners();
  liveinspect.name = "LiveInspect";
  liveinspect.year = 2014;

  var wumdrop = new Winners();
  wumdrop.name = "WumDrop";
  wumdrop.year = 2015;

  var domestly = new Winners();
  domestly.name = "Domestly";
  domestly.year = 2016;

  var shyft = new Winners();
  shyft.name = "Shyft";
  shyft.year = 2017;

  var kula = new Winners();
  kula.name = "Kula ecosystem";
  kula.year = 2018;

  var naked = new Winners();
  naked.name = "Naked Insurance";
  naked.year = 2019;

  var easy = new Winners();
  easy.name = "EasyEquities";
  easy.year = 2020;

  var afri = new Winners();
  afri.name = "Ambani Africa";
  afri.year = 2021;

  List winnerList = [
    snapscan,
    liveinspect,
    wumdrop,
    domestly,
    shyft,
    fnb,
    kula,
    naked,
    easy,
    afri
  ];
  //Sort the list by year
  winnerList.sort((a, b) => a.year.compareTo(b.year));

  //Print list by name per year ascending
  print('\n************The List Of Winners from 2012 to 2021 **************\n');
  for (var winner in winnerList) {
    print(winner.name);
  }
  print('\n************The Winners For 2017 & 2018 **************\n');
//Print the winner of 2017
  for (var winner2017 in winnerList) {
    if (winner2017.year == 2017) {
      print('The Winner For the Year 2017 was: ${winner2017.name}');
    }
  }

//Print the winner of 2018
  for (var winner2018 in winnerList) {
    var year = winner2018.year;
    switch (year) {
      case 2018:
        print('The Winner For the Year 2018 was: ${winner2018.name}');
        break;
    }
  }

  print('\n************Total Number Of Apps**************\n');
  print(
      'The total number of apps That have won since 2012 are: ${winnerList.length}');
}

class Winners {
  String? name;
  int? year;
}
